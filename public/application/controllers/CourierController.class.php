<?php

require_once('BaseSmartyController.class.php');
require_once('Constants.class.php');

/**
 * Short Description of CourierController.
 * 
 * Long description of CourierController.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.1
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 11/03/2013  1.0     Brian Etherington     Initial Version
 * 07/05/2013  1.1     Brian Etherington     Add from_job_booking_page SMARTY variable
 *                                           so that Collection Date and Preferred Courier can be made option
 *                                           if, and only if, the Book Collection form is displayed from the job update page.
 ******************************************************************************/

class CourierController extends BaseSmartyController {
    
    public function __construct() { 
        
        parent::__construct();
        
        if (!isset($this->session->UserID))
            throw new Exception('Unauthorised');
        
    }
    
    public function BookCollectionFormAction( $args ) {
        
        $localised_messages = $this->messages->getPage('bookCollection',$this->lang);
        $this->smarty->assign('page', $localised_messages);
        
        $skyline_business_model = $this->loadModel('SkylineBusinessModel');
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            // POST Request so process the Book Courier Collection Form
            
            if ($_POST['JobID'] == '') {               
                
                // Job booking has not yet been confirmed.
                // Save Collection Booking details for later 
                // when job booking is completed.
                
                $this->session->BookCollectionFormData = $_POST;
                
                // return success
                echo "OK";
            
            } else {
                                              
                $courier_business_model = $this->loadModel('CourierBusinessModel');
                $courier_business_model->BookCourierCollection( $_POST );
                
                echo('OK');
                
            }
            
        } else {
            
            // GET Request so display the Book Courier Collection Form
            

            if (empty($args['job'])) {
                
                $this->smarty->assign('job_id', null);
                $this->smarty->assign('collection', array());
                
                $couriers = array();
                $packaging_types = array();
                
            } else {
                
                $job_details = $skyline_business_model->getJobDetails( $args['job'], $this->user );
                          
                $this->smarty->assign('job_id', $args['job']);                
                $this->smarty->assign('collection', $job_details['collection']); 

                $courier_model = $this->loadModel('Couriers');
                $couriers = $courier_model->getServiceProviderCouriers( $job_details['service_provider']['id'] );
                $packaging_types = $courier_model->getPackagingTypes( $job_details['collection']['courier_id'] );
                
            }
            
            $skyline_model = $this->loadModel('Skyline');
            
            $countries = $skyline_model->getCountries();
            $country_id = 0;
            foreach($countries as $row) {
                if ($row['Name'] == 'UK') {
                    $country_id = $row['CountryID'];
                    break;
                }
            }
            $counties = $skyline_model->getCounties($country_id, Constants::SKYLINE_BRAND_ID);
            
            //$courier_model = $this->loadModel('Couriers');
            //$couriers = $courier_model->getBrandCouriers( $this->user->DefaultBrandID );
            
            $this->smarty->assign('packaging_types', $packaging_types); 
            $this->smarty->assign('couriers', $couriers);
            $this->smarty->assign('countries', $countries);
            $this->smarty->assign('counties', $counties);
            $this->smarty->assign('from_job_booking_page', isset($args['from_job_booking']) ? true : false );
            
            $this->smarty->display('popup/BookCollectionForm.tpl');
            
        }
    }
    

    public function testDPDAction($args) {
        $dpd = $this->loadModel('CourierDPD');

        $dpd->sendSingleSwapitJob($args[0]);
    }
    

    public function PostcodeLookupAction( /* $args */ ) {
        
        $localised_messages = $this->messages->getPage('bookCollection',$this->lang);
        $this->smarty->assign('page', $localised_messages);
        
        $model = $this->loadModel('SkylineBusinessModel');
        $addresses = $model->PostcodeLookup($_POST['Postcode']);
                
        $this->smarty->assign('addresses', $addresses);
               
        echo $this->smarty->fetch('ajax/postcode.tpl');
  
    }
    
    public function CountyLookupAction( /* $args */ ) {
        
        $localised_messages = $this->messages->getPage('bookCollection',$this->lang);
        $this->smarty->assign('page', $localised_messages);
        
        $skyline_model = $this->loadModel('Skyline');
        $counties = $skyline_model->getCounties($_POST['CountryID'], Constants::SKYLINE_BRAND_ID);
        
        $this->smarty->assign('counties', $counties);
        echo $this->smarty->fetch('ajax/counties.tpl');
        
    }
    
    public function PackagingTypesLookupAction( /* $args */ ) {
        
        $localised_messages = $this->messages->getPage('bookCollection',$this->lang);
        $this->smarty->assign('page', $localised_messages);
        
        $courier_model = $this->loadModel('Couriers');
        $packaging_types = $courier_model->getPackagingTypes($_POST['CourierID']);
        
        $this->smarty->assign('packaging_types', $packaging_types);
        echo $this->smarty->fetch('ajax/packagingTypes.tpl');
        
    }
}

?>
