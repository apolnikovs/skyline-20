<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Branches Page in Organisation Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.27
 * 
 * Date        Version Author                 Reason
 * 23/04/2012  1.00    Nageswara Rao Kanteti  Initial Version
 * 24/04/2012  1.01    Nageswara Rao Kanteti  Brands upload logo feature
 * 25/04/2012  1.02    Nageswara Rao Kanteti  Organisation setup amendments as requested by Neil
 * 11/05/2012  1.03    Nageswara Rao Kanteti  Product Setup Pages
 * 16/05/2012  1.04    Nageswara Rao Kanteti  Fixed Problems with Live Database
 * 27/06/2012  1.05    Nageswara Rao Kanteti  New design changes done
 * 02/08/2012  1.06    Nageswara Rao Kanteti  Timeline - Item Location
 * 12/09/2012  1.07    Nageswara Rao Kanteti  TestLog 101
 * 17/09/2012  1.08    Nageswara Rao Kanteti  Sitemap Engineers
 * 08/11/2012  1.09    Vykintas Rutkunas      System Admin facilities vsibility
 * 14/11/2012  1.10    Andrew J. Williams     Added getBranchIdByAccountNoClient
 * 29/11/2012  1.11    Andrew J. Williams     Issue 145 - Error when Creating Branch from API - Undefined property: JobsAPI::$statuses
 * 29/11/2012  1.12    Andrew J. Williams     Changes to allow create method to work from JobsAPI
 * 09/12/2012  1.13    Nageswara Rao Kanteti  branch account number and Policy search (for demo) - done
 * 15/12/2012  1.14    Nageswara Rao Kanteti  Added new filed Contact Fax and fixed jquery issue on branch admin screen.
 * 18/12/2012  1.15    Vykintas Rutkunas      Changes to RA Status Types functionality
 * 19/12/2012  1.16    Nageswara Rao Kanteti  New Phone No & Address Fields in Skyline.docx - done
 * 20/12/2012  1.17    Vykintas Rutkunas      Client Unit Turnaround Time functionality finished
 * 24/12/2012  1.18    Nageswara Rao Kanteti  Job booking issues are fixed requested by Colin.
 * 10/01/2013  1.19    Vykintas Rutkunas      RA functionality work in progress
 * 13/01/2013  1.20    Nageswara Rao Kanteti  Tracker base log No 111, 113, 114, 116, 117 and 119 are done.  And clear preferences feature is done on open and overdue jobs.
 * 23/01/2013  1.21    Nageswara Rao Kanteti  Tracker base Log 144 is done and urgent fixes on job booking
 * 14/02/2013  1.22    Vykintas Rutkunas      Service Appraisal work in progress
 * 22/02/2013  1.23    Vykintas Rutkunas      Job Service Provider allocation changes
 * 22/02/2013  1.24    Nageswara Rao Kanteti  Cancel job email  and Branch Current Location of Product are done.
 * 28/02/2013  1.25    Nageswara Rao Kanteti  Samsung Experience Enhancements in progress.
 * 12/03/2013  1.26    Vykintas Rutkunas      Job Allocation functionality fix
 * 08/04/2013  1.27    Andrew J. Williams     Trackerbase VMS Log 248 - Change Branch Account No to Branch Number 
 ******************************************************************************/

class Branches extends CustomModel {
    
    private $conn;
    private $dbColumns  = array('T1.BranchID', 'T1.BranchName', 'T3.ClientName', 'T1.BranchNumber', 'T1.BranchType', 'T1.BranchID', 'T1.Status');
    private $table  = "branch";
    private $tables = "branch AS T1 LEFT JOIN client_branch AS T2 ON T1.BranchID=T2.BranchID LEFT JOIN client AS T3 ON T2.ClientID=T3.ClientID";
    private $table_client_branch = "client_branch";
    private $table_brand_branch = "brand_branch";
    public  $debug = false;
    
      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        $NetworkID = isset($args['firstArg']) ? $args['firstArg'] : '';
        $ClientID  = isset($args['secondArg']) ? $args['secondArg'] : '';
        
        if($NetworkID!='' || $ClientID!='')
        {
            $sep = '';
            if($NetworkID!='')
            {
                $args['where'] = "T2.NetworkID='".$NetworkID."'";
                $sep = ' AND ';
            }
            
            if($ClientID!='')
            {
                $args['where'] .= $sep."T2.ClientID='".$ClientID."'";
            }
        }
       
        
        $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['BranchID']) || !$args['BranchID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
      /**
     * Description
     * 
     * This method is used for to check whether branch is exist for the client.
     *
     * @param interger $NetworkID  
     * @param interger $ClientID  
     * @param interger $BranchID
     * @param interger $BranchName
     * @global $this->table_client_branch
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($NetworkID, $ClientID, $BranchID, $BranchName) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ClientBranchID FROM '.$this->table.' AS T1 LEFT JOIN '.$this->table_client_branch.' AS T2 ON T1.BranchID=T2.BranchID AND T1.BranchName=:BranchName WHERE T2.NetworkID=:NetworkID AND T2.ClientID=:ClientID AND T1.BranchID!=:BranchID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':NetworkID' => $NetworkID, ':ClientID' => $ClientID, ':BranchID' => $BranchID, ':BranchName' => $BranchName));
        $result = $fetchQuery->fetch();
        
        
      //  $this->controller->log(var_export($sql, true));
      //  $this->controller->log(var_export($result, true));
      //  $this->controller->log(var_export(array(':NetworkID' => $NetworkID, ':ClientID' => $ClientID, ':BranchID' => $BranchID, ':BranchName' => $BranchName), true));
        
                
        if(is_array($result) && $result['ClientBranchID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to check whether brand is exists for the branch.
     *
     * @param interger $BrandID  
     * @param interger $BranchID
     * @global $this->table_brand_branch
     
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isBrandBranchExists($BrandID, $BranchID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT BrandBranchID FROM '.$this->table_brand_branch.' WHERE BrandID=:BrandID AND BranchID=:BranchID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':BrandID' => $BrandID, ':BranchID' => $BranchID));
        $result = $fetchQuery->fetch();
        
                
        if(is_array($result) && $result['BrandBranchID'])
        {
                return true;
        }
        
        return false;
    
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to insert data into database.
    *
    * @param array $args
    * @global $this->table 
    * @global $this->table_client_branch
    * @global $this->table_brand_branch 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function create($args) {
        
        if ($this->isValid($args["NetworkID"], $args["ClientID"], 0, $args["BranchName"])) {
            
	    if (isset($args["SendRepairCompleteTextMessage"]) && $args["SendRepairCompleteTextMessage"] == "Yes") {
		$args["SendRepairCompleteTextMessage"] = "Yes";
	    } else {
		$args["SendRepairCompleteTextMessage"] = "No";
	    }

	    $sql ="INSERT INTO  " . $this->table . " 
				(
				    BranchName, 
				    BuildingNameNumber, 
				    Street, 
				    LocalArea, 
				    TownCity, 
				    CountyID, 
				    CountryID, 
				    PostalCode, 
				    ServiceManager,
				    ContactEmail,
				    ContactPhoneExt, 
				    ContactPhone,
				    ContactFax,
				    BranchNumber, 
				    AccountNo,
				    BranchType, 
				    Status,
				    ServiceAppraisalRequired,
				    DefaultServiceProvider,
				    ThirdPartyServiceProvider,
				    CurrentLocationOfProduct,
				    OriginalRetailerFormElement,
				    CreatedDate, 
				    ModifiedUserID, 
				    ModifiedDate,
				    SendRepairCompleteTextMessage,
				    SMSID,
				    OpenJobsManagement
				)
		    VALUES	    
				(
				    :BranchName, 
				    :BuildingNameNumber, 
				    :Street, 
				    :LocalArea, 
				    :TownCity, 
				    :CountyID, 
				    :CountryID, 
				    :PostalCode, 
				    :ServiceManager,
				    :ContactEmail, 
				    :ContactPhoneExt, 
				    :ContactPhone,
				    :ContactFax,
				    :BranchNumber, 
				    :AccountNo,
				    :BranchType, 
				    :Status, 
				    :ServiceAppraisalRequired,
				    :DefaultServiceProvider,
				    :ThirdPartyServiceProvider,
				    :CurrentLocationOfProduct,
				    :OriginalRetailerFormElement,
				    :CreatedDate, 
				    :ModifiedUserID, 
				    :ModifiedDate,
				    :SendRepairCompleteTextMessage,
				    :SMSID,
				    :OpenJobsManagement
				)";

	    $insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	    if ($this->debug) {
		$this->log("Branches.class create branch insert query = " . $sql);
	    }

	    $result = $insertQuery->execute([
		":BranchName" => (!isset($args["BranchName"]) || $args["BranchName"] == "") ? NULL : $args["BranchName"],
		":BuildingNameNumber" => (!isset($args["BuildingNameNumber"]) || $args["BuildingNameNumber"] == "") ? NULL : $args["BuildingNameNumber"],
		":Street" => (!isset($args["Street"]) || $args["Street"] == "") ? NULL : $args["Street"],
		":LocalArea" => (!isset($args["LocalArea"]) || $args["LocalArea"] == "") ? NULL : $args["LocalArea"],
		":TownCity" => (!isset($args["TownCity"]) || $args["TownCity"] == "") ? NULL : $args["TownCity"],
		":CountyID" => (!isset($args["CountyID"]) || $args["CountyID"] == "") ? NULL : $args["CountyID"],
		":CountryID" => (!isset($args["CountryID"]) || $args["CountryID"] == "") ? NULL : $args["CountryID"],
		":PostalCode" => (!isset($args["PostalCode"]) || $args["PostalCode"] == "") ? NULL : $args["PostalCode"],
		":ContactEmail" => (!isset($args["ContactEmail"]) || $args["ContactEmail"] == "") ? NULL : $args["ContactEmail"],
		":ServiceManager" => (!isset($args["ServiceManager"]) || $args["ServiceManager"] == "") ? NULL : $args["ServiceManager"],
		":ContactPhoneExt" => (!isset($args["ContactPhoneExt"]) || $args["ContactPhoneExt"] == "") ? NULL : $args["ContactPhoneExt"],
		":ContactPhone" => (!isset($args["ContactPhone"]) || $args["ContactPhone"] == "") ? NULL : $args["ContactPhone"],
		":ContactFax" => (!isset($args["ContactFax"]) || $args["ContactFax"] == "") ? NULL : $args["ContactFax"],
		":BranchNumber" => (!isset($args["BranchNumber"]) || $args["BranchNumber"] == "") ? NULL : $args["BranchNumber"],
		":AccountNo" => (!isset($args["AccountNo"]) || $args["AccountNo"] == "") ? NULL : $args["AccountNo"],
		":BranchType" => (!isset($args["BranchType"]) || $args["BranchType"] == "") ? NULL : $args["BranchType"],
		":Status" => (!isset($args["Status"]) || $args["Status"] == "") ? "Active" : $args["Status"],
		":ServiceAppraisalRequired" => $args["ServiceAppraisalRequired"],
		":DefaultServiceProvider" => ((isset($args["DefaultServiceProvider"]) && $args["DefaultServiceProvider"] != "") ? $args["DefaultServiceProvider"] : null),
		":ThirdPartyServiceProvider" => ((isset($args["ThirdPartyServiceProvider"]) && $args["ThirdPartyServiceProvider"] != "") ? $args["ThirdPartyServiceProvider"] : null),
		":CurrentLocationOfProduct" => ((isset($args["CurrentLocationOfProduct"]) && $args["CurrentLocationOfProduct"] != "") ? $args["CurrentLocationOfProduct"] : null),
		":OriginalRetailerFormElement" => ((isset($args["OriginalRetailerFormElement"]) && $args["OriginalRetailerFormElement"] != "") ? $args["OriginalRetailerFormElement"] : null),
		":CreatedDate" => date("Y-m-d H:i:s"),
		":ModifiedUserID" => $this->controller->user->UserID,
		":ModifiedDate" => date("Y-m-d H:i:s"),
		":SendRepairCompleteTextMessage" => $args["SendRepairCompleteTextMessage"],
		":SMSID" => (isset($args["SMSID"])) ? $args["SMSID"] : NULL,
		":OpenJobsManagement" => (isset($args["OpenJobsManagement"])) ? $args["OpenJobsManagement"] : NULL
	    ]);

	    if ($this->debug) {
		$this->log("Branches.class create branch insert query result= " . var_export($result, true));
	    }

	    $BranchID = $this->conn->lastInsertId();

	    $result2 = false; 
	    $result3 = false; 

	    //If the branch details are inserted into database then we are inserting relation into table $this->table_client_branch and $this->table_brand_branch	
	    if ($result && $BranchID) {
		if ($this->debug) {
		    $this->log("Branches.class create Created BranchID " . $BranchID);
		}
		//Inserting details into $this->table_client_branch
		/* Execute a prepared statement by passing an array of values */
		$sql2 = "	INSERT INTO " . $this->table_client_branch." 
					(
					    ClientID, 
					    BranchID, 
					    NetworkID, 
					    CreatedDate, 
					    Status, 
					    ModifiedUserID, 
					    ModifiedDate
					)
			    VALUES
					(
					    :ClientID, 
					    :BranchID, 
					    :NetworkID, 
					    :CreatedDate, 
					    :Status, 
					    :ModifiedUserID, 
					    :ModifiedDate
					)";

		$insertQuery2 = $this->conn->prepare($sql2, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

		if ($this->debug) {
		    $this->log("Branches.class create client_branch insert query = " . $sql2);
		}
		$result2 = $insertQuery2->execute([
		    ":ClientID" => $args["ClientID"],
		    ":BranchID" => $BranchID,
		    ":NetworkID" => $args["NetworkID"],
		    ":CreatedDate" => date("Y-m-d H:i:s"),
		    ":Status" => $args["Status"],
		    ":ModifiedUserID" => $this->controller->user->UserID,
		    ":ModifiedDate" => date("Y-m-d H:i:s")
		]);

		if ($this->debug) {
		    $this->log("Branches.class create client_branch insert query result= " . var_export($result2, true));
		}
		//Inserting details into $this->table_brand_branch
		/* Execute a prepared statement by passing an array of values */
		$sql3 = "	INSERT INTO " . $this->table_brand_branch . " 
					(
					    BranchID, 
					    BrandID, 
					    CreatedDate, 
					    Status, 
					    ModifiedUserID, 
					    ModifiedDate
					)
			    VALUES	    
					(
					    :BranchID, 
					    :BrandID, 
					    :CreatedDate, 
					    :Status, 
					    :ModifiedUserID, 
					    :ModifiedDate
					)";

		if ($this->debug) {
		    $this->log("Branches.class create client_branch insert query = " . $sql3);
		}

		$insertQuery3 = $this->conn->prepare($sql3, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

		if (!is_array($args["BrandID"]) && $args["BrandID"]) {
		    $temp_brand = $args["BrandID"];
		    $args["BrandID"] = "";
		    $args["BrandID"][0] = $temp_brand; 
		}    

		if (is_array($args["BrandID"])) {
		    for ($b = 0; $b < count($args["BrandID"]); $b++) {
			if (isset($this->controller->statuses[0]["Code"])) {
			    $statusQ3 = $this->controller->statuses[0]["Code"];
			} else {
			    $statusQ3 = "Active";
			}
			$result3 = $insertQuery3->execute([
			    ":BrandID" => $args["BrandID"][$b], 
			    ":BranchID" => $BranchID, 
			    ":CreatedDate" => date("Y-m-d H:i:s"),
			    ":Status" => $statusQ3,
			    ":ModifiedUserID" => $this->controller->user->UserID,
			    ":ModifiedDate" => date("Y-m-d H:i:s")
			]);
			if ($this->debug) {
			    $this->log("Branches.class create brand_branch insert query result= " . var_export($result3, true));
			}
		    }
		}   

	    }

	    if ($result && $result2) {

		if (isset($_SESSION["newBranchAddedMailFlag"]) && $_SESSION["newBranchAddedMailFlag"]) {
		    //Sending mail to skyline dev issue.
		    $mail_subject = trim($this->controller->user->ContactFirstName . " " . $this->controller->user->ContactLastName) . " (UserID: " . $this->controller->user->UserID . ") Created Branch - ClientID: " . $args["ClientID"] . " - " . $args["BranchName"] . " (BranchID: " . $BranchID . "): " . $args["BranchNumber"];

		    @mail($this->controller->config["General"]["skyline_issue_email"], $mail_subject, $mail_subject);

		    $this->controller->log("Email Sent:");
		    $this->controller->log(var_export($this->controller->config["General"]["skyline_issue_email"], true));
		    $this->controller->log(var_export($mail_subject, true));

		    $_SESSION['newBranchAddedMailFlag'] = false;
		}    

		/* Messages below are dependent on controller reading $page variable the API dones not do this.
		 * Having the model do this breaks the MVC design standards - the model should have passed generic codes
		 * back to the controller which then should look up any apporiate text. For conrollers that controll this model
		 * but do not access the %page variable we set stome default text.
		 */

		if (isset($this->controller->page["Text"]["data_inserted_msg"])) {
		    $retMsg = $this->controller->page["Text"]["data_inserted_msg"];
		} else {
		    $retMsg = "Data inserted";
		}

		return [
		    "status" => "OK",
		    "message" => $retMsg,
		    "BranchID" => $BranchID
		];

	    } else {

		if (isset($this->controller->page["Errors"]["data_not_processed"])) {
		    $retMsg = $this->controller->page["Errors"]["data_not_processed"];
		} else {
		    $retMsg = "Data not processed";
		}

		return [
		    "status" => "ERROR",
		    "message" => $retMsg,
		    "BranchID" => $BranchID
		];
	    }
	    
	} else {
	    
            if (isset($this->controller->messages) && isset($this->controller->lang)) {
                $retMsg = $this->controller->messages->getError(1024, "default", $this->controller->lang);
            } else {
                $retMsg = "Error 1024";
            }
	    return [
		"status" => "ERROR",
		"message" => $retMsg,
		"BranchID" => null
	    ];
	    
        }
	
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @global $this->table_client_branch
     * @global $this->table_brand_branch
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT	BranchID, 
			BranchName, 
			BuildingNameNumber, 
			Street, 
			LocalArea, 
			TownCity, 
			CountyID, 
			CountryID, 
			PostalCode,
			ServiceManager,
			ContactEmail, 
			ContactPhoneExt, 
			ContactPhone,
                        ContactFax,
			BranchNumber,
                        AccountNo,
			BranchType, 
			Status,
			ServiceAppraisalRequired,
			DefaultServiceProvider,
			ThirdPartyServiceProvider,
                        CurrentLocationOfProduct,
                        OriginalRetailerFormElement,
			CreatedDate, 
			ModifiedUserID, 
			ModifiedDate,
                        SendRepairCompleteTextMessage,
                        SMSID,
			OpenJobsManagement
			
		FROM	' . $this->table . ' 
		    
		WHERE	BranchID=:BranchID';
	
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        $fetchQuery->execute(array(':BranchID' => $args['BranchID']));
        $result = $fetchQuery->fetch();
        
        if(is_array($result))
        {
            //Getting client and network details.
            $sql2        = 'SELECT ClientID, NetworkID, ClientBranchID FROM '.$this->table_client_branch.' WHERE BranchID=:BranchID';
            $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery2->execute(array(':BranchID' => $args['BranchID']));
            $result2     = $fetchQuery2->fetch();
            
            $result['ClientID']  = isset($result2['ClientID'])?$result2['ClientID']:'';
            $result['NetworkID'] = isset($result2['NetworkID'])?$result2['NetworkID']:'';
            $result['ClientBranchID'] = isset($result2['ClientBranchID'])?$result2['ClientBranchID']:'';
            
            
            //Getting Brand details of this branch.
            $sql3        = 'SELECT BrandID FROM '.$this->table_brand_branch.' WHERE BranchID=:BranchID AND Status=:Status';
            $fetchQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery3->execute(array(':BranchID' => $args['BranchID'], ':Status' => 'Active'));
           $result['BrandID'] = array();
            
            while($result3     = $fetchQuery3->fetch())
            {
                 if(isset($result3['BrandID']) && $result3['BrandID'])
                 {
                    $result['BrandID'][]  = $result3['BrandID'];
                 }
            }
        }
        
       // $this->controller->log(var_export($result, true));
        
        return $result;
     }
     
     
     
     
     
     /**
     * Description
     * 
     * This method is used for to fetch branches with addresses for given client and network.
     *
     * @param array $ClientID
     * @param array $NetworkID 
     * @return array It contains address details of all branches.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     
     public function getBranchesWithAddress($ClientID, $NetworkID, $BranchID=null){
        
        $result = array();
       
        if($BranchID)
        {
            $sql = "SELECT T1.BranchID, T1.BranchName, T1.BuildingNameNumber, T1.Street, T1.LocalArea, T1.TownCity, T1.CountyID, T1.CountryID, T1.PostalCode, T1.ContactPhone, T1.ContactPhoneExt, T1.ContactEmail FROM branch AS T1 WHERE T1.Status='Active' AND T1.BranchID=:BranchID ORDER BY T1.BranchName" ;
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':BranchID' => $BranchID));
           
            $result = $fetchQuery->fetch();
            
        }
        else if($ClientID && $NetworkID)
        {
            
            $sql = "SELECT T1.BranchID, T1.BranchName, T1.BuildingNameNumber, T1.Street, T1.LocalArea, T1.TownCity, T1.CountyID, T1.CountryID, T1.PostalCode, T1.ContactPhone, T1.ContactPhoneExt, T1.ContactEmail FROM branch AS T1 LEFT JOIN client_branch AS T2 ON T1.BranchID=T2.BranchID WHERE T1.Status='Active' AND T2.Status='Active' AND T2.ClientID=:ClientID AND T2.NetworkID=:NetworkID ORDER BY T1.BranchName" ;
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':NetworkID' => $NetworkID, ':ClientID' => $ClientID));
           
            $result = $fetchQuery->fetchAll();
        }
        
        return $result;
    }   
     
     
     
    /**
     * Description
     * 
     * This method is used for to fetch brand, brach name and town details of given BranchID.
     *
     * @param array $BranchID
     * @return array It contains brand, brach name and town branch.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     
     public function getBranchWithBrand($BranchID){
        
        $result = array();
       
        if($BranchID)
        {
            
            $sql = "SELECT T3.BrandName, T1.BranchName, T1.TownCity, T1.BranchType FROM branch AS T1 LEFT JOIN brand_branch AS T2 ON T1.BranchID=T2.BranchID LEFT JOIN brand AS T3 ON T2.BrandID=T3.BrandID WHERE T1.BranchID=:BranchID AND T1.Status='Active' AND T2.Status='Active' AND T3.Status='Active' LIMIT 0,1" ;
              
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':BranchID' => $BranchID));
           
            $result = $row = $fetchQuery->fetch();
        }
        
        return $result;
    }
     
     
     
     
     
     /**
     * Description
     * 
     * This method is used to get the name of Branch.
     *
     * @param int $BranchID
     * @global $this->table  
     * @return string 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function name($BranchID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT BranchName FROM '.$this->table.' WHERE BranchID=:BranchID AND Status=:Status';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        $fetchQuery->execute(array(':BranchID' => $BranchID, ':Status' => 'Active'));
        $result = $fetchQuery->fetch();
        
        return isset($result['BranchName'])?$result['BranchName']:'';
     }
     

     
    /**
    * Description
    * 
    * This method is used for to udpate a row into database.
    *
    * @param array $args
    * @global $this->table   
    * @global $this->table_client_branch
    * @global $this->table_brand_branch 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function update($args) {
        
	if ($this->isValid($args["NetworkID"], $args["ClientID"], $args["BranchID"], $args["BranchName"])) {        
	    $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if ($this->controller->statuses[1]["Code"] == $args["Status"]) {
		if ($row_data["Status"] != $args["Status"]) {
		    $EndDate = date("Y-m-d H:i:s");
		}
            }
            
            if (isset($args["SendRepairCompleteTextMessage"]) && $args["SendRepairCompleteTextMessage"] == "Yes") {
                $args["SendRepairCompleteTextMessage"] = "Yes";
            } else {
                $args["SendRepairCompleteTextMessage"] = "No";
            }
            
            $sql = "UPDATE  " . $this->table . " 
		
		    SET	    BranchName = :BranchName, 
			    BuildingNameNumber = :BuildingNameNumber, 
			    Street = :Street, 
			    LocalArea = :LocalArea, 
			    TownCity = :TownCity, 
			    CountyID = :CountyID, 
			    CountryID = :CountryID, 
			    PostalCode = :PostalCode,
			    ServiceManager = :ServiceManager,
			    ContactEmail = :ContactEmail, 
			    ContactPhoneExt = :ContactPhoneExt, 
			    ContactPhone = :ContactPhone,
                            ContactFax  = :ContactFax,
			    BranchNumber = :BranchNumber, 
                            AccountNo = :AccountNo,
			    BranchType = :BranchType, 
			    Status = :Status,
			    ServiceAppraisalRequired = :ServiceAppraisalRequired,
			    DefaultServiceProvider = :DefaultServiceProvider,
			    ThirdPartyServiceProvider = :ThirdPartyServiceProvider,
                            CurrentLocationOfProduct = :CurrentLocationOfProduct,
                            OriginalRetailerFormElement = :OriginalRetailerFormElement,
			    EndDate = :EndDate, 
			    ModifiedUserID = :ModifiedUserID, 
			    ModifiedDate = :ModifiedDate, 
                            SendRepairCompleteTextMessage = :SendRepairCompleteTextMessage,
                            SMSID = :SMSID,
			    OpenJobsManagement = :OpenJobsManagement
			    
		    WHERE   BranchID = :BranchID
		 ";
            
	    $updateQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	    $result = $updateQuery->execute([
		":BranchName" => (!isset($args["BranchName"]) || $args["BranchName"] == "") ? NULL : $args["BranchName"], 
		":BuildingNameNumber" => (!isset($args["BuildingNameNumber"]) || $args["BuildingNameNumber"] == "") ? NULL : $args["BuildingNameNumber"], 
		":Street" => (!isset($args["Street"]) || $args["Street"] == "") ? NULL : $args["Street"],
		":LocalArea" => (!isset($args["LocalArea"]) || $args["LocalArea"] == "") ? NULL : $args["LocalArea"],
		":TownCity" => (!isset($args["TownCity"]) || $args["TownCity"] == "") ? NULL : $args["TownCity"],
		":CountyID" => (!isset($args["CountyID"]) || $args["CountyID"] == "") ? NULL:$args["CountyID"],
		":CountryID" => (!isset($args["CountryID"]) || $args["CountryID"] == "") ? NULL:$args["CountryID"],
		":PostalCode" => (!isset($args["PostalCode"]) || $args["PostalCode"] == "") ? NULL : $args["PostalCode"],
		":ServiceManager" => (!isset($args["ServiceManager"]) || $args["ServiceManager"] == "") ? NULL : $args["ServiceManager"],
		":ContactEmail" => (!isset($args["ContactEmail"]) || $args["ContactEmail"] == "") ? NULL : $args["ContactEmail"],
		":ContactPhoneExt" => (!isset($args["ContactPhoneExt"]) || $args["ContactPhoneExt"] == "") ? NULL : $args["ContactPhoneExt"],
		":ContactPhone" => (!isset($args["ContactPhone"]) || $args["ContactPhone"] == "") ? NULL : $args["ContactPhone"],
		":ContactFax" => (!isset($args["ContactFax"]) || $args["ContactFax"] == "") ? NULL : $args["ContactFax"],
		":BranchNumber" => (!isset($args["BranchNumber"]) || $args["BranchNumber"] == "") ? NULL : $args["BranchNumber"],
		":AccountNo" => (!isset($args["AccountNo"]) || $args["AccountNo"] == "") ? NULL : $args["AccountNo"],
		":BranchType" => (!isset($args["BranchType"]) || $args["BranchType"] == "") ? NULL : $args["BranchType"],
		":Status" => (!isset($args["Status"]) || $args["Status"] == "") ? "Active" : $args["Status"],
		":ServiceAppraisalRequired" => $args["ServiceAppraisalRequired"],
		":DefaultServiceProvider" => ((isset($args["DefaultServiceProvider"]) && $args["DefaultServiceProvider"] != "") ? $args["DefaultServiceProvider"] : null),
		":ThirdPartyServiceProvider" => ((isset($args["ThirdPartyServiceProvider"]) && $args["ThirdPartyServiceProvider"] != "") ? $args["ThirdPartyServiceProvider"] : null),
		":CurrentLocationOfProduct" => ((isset($args["CurrentLocationOfProduct"]) && $args["CurrentLocationOfProduct"] != "") ? $args["CurrentLocationOfProduct"] : null),
		":OriginalRetailerFormElement" => ((isset($args["OriginalRetailerFormElement"]) && $args["OriginalRetailerFormElement"] != "") ? $args["OriginalRetailerFormElement"] : null),
		":EndDate" => $EndDate,
		":ModifiedUserID" => $this->controller->user->UserID,
		":ModifiedDate" => date("Y-m-d H:i:s"),
		":BranchID" => $args["BranchID"],
		":SendRepairCompleteTextMessage" => $args["SendRepairCompleteTextMessage"],
		":SMSID" => (isset($args["SMSID"])) ? $args["SMSID"] : NULL,
		":OpenJobsManagement" => (isset($args["OpenJobsManagement"])) ? $args["OpenJobsManagement"] : NULL
	    ]);
        
	    $result2 = false; 
	    $result3 = false; 
	    
	    //If the branch details are updated into database then we are updating relation into table $this->table_client_branch and $this->table_brand_branch
	    if ($result) {
                   
		//Updating details into $this->table_client_branch
		$sql2 = "   UPDATE  " . $this->table_client_branch . " 
			    SET	    ClientID = :ClientID, 
				    BranchID = :BranchID, 
				    NetworkID = :NetworkID, 
				    EndDate = :EndDate, 
				    Status = :Status, 
				    ModifiedUserID = :ModifiedUserID, 
				    ModifiedDate = :ModifiedDate 
				    
			    WHERE   ClientBranchID = :ClientBranchID
			";

		/* Execute a prepared statement by passing an array of values */
		$updateQuery2 = $this->conn->prepare($sql2, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

		$result2 = $updateQuery2->execute([
		    ":ClientID" => $args["ClientID"], 
		    ":BranchID" => $args["BranchID"], 
		    ":NetworkID" => $args["NetworkID"],
		    ":EndDate" => $EndDate,
		    ":Status" => $args["Status"],
		    ":ModifiedUserID" => $this->controller->user->UserID,
		    ":ModifiedDate" => date("Y-m-d H:i:s"),
		    ":ClientBranchID" => $row_data["ClientBranchID"]
		]);
                        
		//In-activating all brands of this branch in table $this->table_brand_branch

		//First we are in-activating all brands of this branch.
		$sql3_1 = " UPDATE  " . $this->table_brand_branch . " 
		    
			    SET	    EndDate = :EndDate, 
				    Status = :Status, 
				    ModifiedUserID = :ModifiedUserID, 
				    ModifiedDate = :ModifiedDate 
				    
			    WHERE   BranchID = :BranchID
			  ";

		/* Execute a prepared statement by passing an array of values */    
		$updateQuery3_1 = $this->conn->prepare($sql3_1, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

		$updateQuery3_1->execute([
		    ":EndDate" => date("Y-m-d H:i:s"),
		    ":Status" => $this->controller->statuses[1]["Code"],
		    ":ModifiedUserID" => $this->controller->user->UserID,
		    ":ModifiedDate" => date("Y-m-d H:i:s"),
		    ":BranchID" => $args["BranchID"] 
		]);
                        
		//Updating details into $this->table_brand_branch
		$sql3 = "   UPDATE  " . $this->table_brand_branch . " 
		    
			    SET	    EndDate = :EndDate, 
				    Status = :Status, 
				    ModifiedUserID = :ModifiedUserID, 
				    ModifiedDate = :ModifiedDate
				    
			    WHERE   BranchID = :BranchID AND BrandID = :BrandID
			";

		/* Execute a prepared statement by passing an array of values */    
		$updateQuery3 = $this->conn->prepare($sql3, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
                        
		//Inserting details into $this->table_brand_branch
		/* Execute a prepared statement by passing an array of values */
		$sql4 = "   INSERT INTO	" . $this->table_brand_branch . " 
					(
					    BranchID, 
					    BrandID, 
					    CreatedDate, 
					    Status, 
					    ModifiedUserID, 
					    ModifiedDate
					)
			    VALUES
					(
					    :BranchID, 
					    :BrandID, 
					    :CreatedDate, 
					    :Status, 
					    :ModifiedUserID, 
					    :ModifiedDate
					)";

		$insertQuery4 = $this->conn->prepare($sql4, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
                        
		if (!is_array($args["BrandID"]) && $args["BrandID"]) {
		    $temp_brand = $args["BrandID"];
		    $args["BrandID"] = "";
		    $args["BrandID"][0] = $temp_brand;
		}        
                        
		if (is_array($args['BrandID'])) {
		    
		    for ($b = 0; $b < count($args["BrandID"]); $b++) {
                                
			$existsResult = $this->isBrandBranchExists($args["BrandID"][$b], $args["BranchID"]);
                                
			if ($existsResult) {
			    
			    $updateQuery3->execute([
				":BrandID" => $args["BrandID"][$b], 
				":EndDate" => date("Y-m-d H:i:s"),
				":Status" => $this->controller->statuses[0]["Code"],
				":ModifiedUserID" => $this->controller->user->UserID,
				":ModifiedDate" => date("Y-m-d H:i:s"),
				":BranchID" => $args["BranchID"] 
			    ]);
			    
			} else {
                                    
			    $insertQuery4->execute([
				":BrandID" => $args["BrandID"][$b], 
				":BranchID" =>  $args["BranchID"], 
				":CreatedDate" => date("Y-m-d H:i:s"),
				":Status" => $this->controller->statuses[0]["Code"],
				":ModifiedUserID" => $this->controller->user->UserID,
				":ModifiedDate" => date("Y-m-d H:i:s")
			    ]);

			}
			
		    }
		    
		} 
                        
	    }
              
	    if ($result && $result2) {
		return [
		    "status" => "OK",
		    "message" => $this->controller->page["Text"]["data_updated_msg"]
		];
	    } else {
		return [
		    "status" => "ERROR",
		    "message" => $this->controller->page["Errors"]["data_not_processed"]
		];
	    }
             
	} else {
	    
	    return [
		"status" => "ERROR",
		"message" => $this->controller->messages->getError(1024, "default", $this->controller->lang)
	    ];
	     
        }
	
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to udpate address.
    *
    * @param array $args
    * @global $this->table   
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function updateAddress($args) {
        
        if($args['BranchID']) {        
            
            $sql = 'UPDATE  ' . $this->table . ' 
		
		    SET	    BuildingNameNumber = :BuildingNameNumber, 
			    Street = :Street, 
			    LocalArea = :LocalArea, 
			    TownCity = :TownCity, 
			    CountyID = :CountyID, 
			    CountryID = :CountryID, 
			    PostalCode = :PostalCode,
			    ContactEmail = :ContactEmail, 
			    ContactPhoneExt = :ContactPhoneExt, 
			    ContactPhone = :ContactPhone,
                            ModifiedUserID = :ModifiedUserID, 
			    ModifiedDate = :ModifiedDate 
			    
		    WHERE   BranchID=:BranchID';
            
	    $updateQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
	    
	    $result = $updateQuery->execute([                          
		':BuildingNameNumber' => $args['BuildingNameNumber'], 
		':Street' => $args['Street'],
		':LocalArea' => $args['LocalArea'],
		':TownCity' => $args['TownCity'],
		':CountyID' => ($args['CountyID'] == '') ? NULL : $args['CountyID'],
		':CountryID' => ($args['CountryID'] == '') ? NULL : $args['CountryID'],
		':PostalCode' => $args['PostalCode'],
		':ContactEmail' => $args['ContactEmail'],
		':ContactPhoneExt' => $args['ContactPhoneExt'],
		':ContactPhone' => $args['ContactPhone'],
		':ModifiedUserID' => $this->controller->user->UserID,
		':ModifiedDate' => date("Y-m-d H:i:s"),
		':BranchID' => $args['BranchID'],  
	    ]);
        
	    if($result) {
		return ['status' => 'OK',
			'message' => "Your data has been updated successfully."];
	    } else {
		return ['status' => 'ERROR',
			'message' => "Sorry, your data has not been processed since there is a problem. Please check it."];
	    }
              
        } else {
	    return ['status' => 'ERROR',
		    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang)];
        }
	
    }
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    /**
     * getBranchClientNetwork
     * 
     * Return the client and network for a branch
     * 
     * @param integer $BranchID     A specific client ID to be searched for
     *                              
     * 
     * @return array $result    Array with Client and NetworkID
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com>  
     *************************************************************************/
    
     public function getBranchClientNetwork($BranchID) {
                
          $sql = "
                  SELECT 
			cb.`ClientID`,
                        cb.`NetworkID`
                  FROM 
			`client_branch` AS cb LEFT JOIN `branch` AS b ON cb.BranchID = b.BranchID 
                  WHERE
			b.Status = 'Active'
                        AND cb.`BranchID` = $BranchID
                 ";
          
          $query = $this->conn->query($sql);
          
          $row = $query->fetch();
           
          $result = array(
                          "ClientID"=>$row['ClientID'],
                          "NetworkID"=>$row['NetworkID']
                         );
                          
          return ($result);       
    }
    
    
    /**
     * getBranchIdByAccountNoClient
     * 
     * Get the branch id with aspecific account number exists for the given 
     * client ID
     * 
     * @param integer $bAcNo    Branch Account Number
     * @param integer $cId      Client ID                   
     * 
     * @return integer      Branch ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     *************************************************************************/
    
     public function getBranchIdByAccountNoClient($bAcNo, $cId) {
         $sql = "
                 SELECT
                	b.`BranchID`
                 FROM 
                	`branch` b,
                        `client_branch` cb
                 WHERE
                        b.`BranchID` = cb.`BranchID`
                        AND cb.`ClientID` = $cId
                        AND b.`AccountNo` = '$bAcNo'
                ";
         
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['BranchID']);                                     /* Branch exists so return ststus name */
        } else {
            return(null);                                                       /* Not found return null */
        }
     }
    
     
    /**
     * getBranchIdByBranchNoClient
     * 
     * Get the branch id with a specific branch number exists for the given 
     * client ID
     * 
     * @param integer $bNo      Branch Number
     * @param integer $cId      Client ID                   
     * 
     * @return integer      Branch ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     *************************************************************************/
    
     public function getBranchIdByBranchNoClient($bNo, $cId) {
         $sql = "
                 SELECT
                	b.`BranchID`
                 FROM 
                	`branch` b,
                        `client_branch` cb
                 WHERE
                        b.`BranchID` = cb.`BranchID`
                        AND cb.`ClientID` = $cId
                        AND b.`BranchNumber` = '$bNo'
                ";
         
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['BranchID']);                                     /* Branch exists so return ststus name */
        } else {
            return(null);                                                       /* Not found return null */
        }
     }
     
     
     public function getBranchByID($branchID) {
	 
	 $q = "SELECT * FROM branch WHERE BranchID = :branchID";
	 $values = ["branchID" => $branchID];
	 $result = $this->query($this->conn, $q, $values);
	 return $result;
	 
     }
     
     
     
     /**
     @action returns branch.DefaultServiceProvider or 0
     @input  branch ID
     @output void
     @return int
     */
     
     public function getBranchServiceProvider($branchID) {
	 
	 $q = "SELECT DefaultServiceProvider FROM branch WHERE branchID = :branchID";
	 $values = ["branchID" => $branchID];
	 $result = $this->query($this->conn, $q, $values);
	 return ((isset($result[0]) && $result[0]["DefaultServiceProvider"] != null) ? $result[0]["DefaultServiceProvider"] : 0);
	 
     }
     
     
     
     /**
     @action returns branch.ThirdPartyServiceProvider or 0
     @input  branch ID
     @output void
     @return int
     */
     
     public function getBranchThirdPartyServiceProvider($branchID) {
	 
	 $q = "SELECT ThirdPartyServiceProvider FROM branch WHERE branchID = :branchID";
	 $values = ["branchID" => $branchID];
	 $result = $this->query($this->conn, $q, $values);
	 return (isset($result[0]) ? $result[0]["ThirdPartyServiceProvider"] : 0);
	 
     }

     
     
}
?>
