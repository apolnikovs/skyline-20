<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
require_once('Functions.class.php');
require_once('Job.class.php');

/**
 * Short Description 
 * 
 * Long description 
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.1
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 12/04/2013  1.0     Brian Etherington     Initial Version
 * 24/03/2013  1.1     Brian Etherington     CorrectTypos in DeleteData and GetAttributeNames
 * **************************************************************************** */

class Questionnaire extends CustomModel {
    
    private $conn;
    private $table;
    private $table_log;
    private $table_attribute;
    private $table_data;
    private $attributes = null;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::Questionnaire();
        $this->table_log = TableFactory::QuestionnaireLog();
        $this->table_data = TableFactory::QuestionnaireData();
        $this->table_attribute = TableFactory::QuestionnaireAttribute();

    }
    
    /**
     * select rows from table.
     *  
     * @param array $params
     */
     
    public function Select( $sql, $params=null ) {
        return $this->Query( $this->conn, $sql, $params );
    }
    
    /**
     * Add row to table.
     *  
     * @param array $params
     */
     
    public function Add( $params ) {
        return $this->InsertRow( $this->conn, $this->table, $params );
    }
    
    /**
     * Update row in table.
     *  
     * @param array $params
     */
     
    public function Update( $params=array() ) {
        return $this->UpdateRow( $this->conn, $this->table, $params );
    }
    
    /**
     * Delete row(s) from table.
     *  
     * @param array/string $where
     */
     
    public function Delete( $params ) {
        return $this->DeleteRow( $this->conn, $this->table, $params );
    }
    
    /******************************************************************
     * 
     * Questionnaire Data Methods
     * 
     ******************************************************************/
    
    public function GetData( $id, $attribute ) {
        
        $attribute_id = $this->GetAttributeID( $attribute );
        
        $sql = "select * from questionnaire_date where QuestionnaireID=:QuestionnaireID and AttributeID=:AttributeID";
        $params = array(
            'QuestionnaireID' => $id,
            'AttributeID' => $attribute_id
        );
        $result = $this->Query($this->conn, $sql, $params);
        
        if (count($result) == 0)
            return null;
        
        if ($result[0]['NumericValue'] !== null)
            return $result[0]['NumericValue'];
        
        if ($result[0]['TextValue'] !== null)
            return $result[0]['TextValue'];
        
        return $result[0]['StringValue'];        
    }
    
    public function AddData( $id, $attribute, $value ) {
        
        $attribute_id = $this->GetAttributeID( $attribute );
        
        if ($attribute_id == null)
            $attribute_id = $this->AddAttribute( $attribute );
        
        $params = array( 'QuestionnaireID' => $id,
                         'AttributeID' => $attribute_id );
        
        if ( is_numeric($value))
            $params['NumericValue'] = $value;
        else if ( strlen($value) > 60 )
            $params['TextValue']  = $value;
        else 
            $params['StringValue'] = $value; 
        
        return $this->InsertRow( $this->conn, $this->table_data, $params );
        
    }
    
    public function DeleteData( $id ) {
        $where = "where QuestionnaireID=$id";
        return $this->DeleteRow( $this->conn, $this->table, $where );
    }
    
    /******************************************************************
     * 
     * Questionnaire Attribute Methods
     * 
     ******************************************************************/
    
    public function GetAttributeNames() {
        $sql = "select AttributeName from questionnaire_attribute order by AttributeName";
        $result = $this->Query($this->conn, $sql);
        $names = array();
        foreach ($result as $row) {
            $names[] = $row['AttributeName'];
        }
        return $names;
    }
    
    public function GetAttributes() {
        $sql = "select * from questionnaire_attribute order by AttributeName";
        return $this->Query($this->conn, $sql);
    }
    
    public function GetAttributeID( $attribute ) {
        
        if ( $this->attributes == null )
            $this->attributes = $this->GetAttributes();
        
        foreach($this->attributes as $row) {
            if ( $attribute == $row['AttributeName'])
                return $row['QuestionnaireAttributeID'];
        }
        
        return null;
     }
    
    public function AddAttribute( $name ) {
        return $this->InsertRow( $this->conn, $this->table_attribute, array('AttributeName' => $name) );
    }
    
    /******************************************************************
     * 
     * Questionnaire log Methods
     * 
     ******************************************************************/
    
    public function AddLog( $JobID, $Brand, $Type ) {
        $guid = Functions::CreateGuid();
        $params = array (
            'QuestionnaireLog' => $guid,
            'JobID' => $JobID,
            'BrandID' => $Brand,
            'Type' =>$Type
        );
        $this->InsertRow( $this->conn, $this->table_log, $params );
        return $guid;
    }
    
    public function GetLog( $id ) {
        $sql = "select * from questionnaire_log where QuestionnaireLog=:ID";
        $params = array( 'ID' => $id );
        $result = $this->Query($this->conn, $sql, $params);
        if (count($result) == 0)
            return null;
        return $result[0];
    }
    
    public function MarkLog( $id ) {
        $params = array(
            'QuestionnaireLog' => $id,
            'Response' => 'Yes'
        );
        return $this->UpdateRow( $this->conn, $this->table_log, $params );
    }
    
    public function DeleteLog( $id ) {
        $params = array( 'QuestionnaireLog' => $id);
        return $this->DeleteRow( $this->conn, $this->table_log, $params );
    }
   
     /**
     * satisfactionary Questionnaires      
     * Check customer is already submitted the questionaire.. avoiding double time                                       
     * submission of same quesionaire 
     * 
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/   
    
   public function ValidateCustomerQuestionaire( $logid ) {
        $sql = "select * from questionnaire_log where QuestionnaireLog=:ID AND Response='Yes' ";
        $params = array( 'ID' => $logid );
        $result = $this->Query($this->conn, $sql, $params);
        if (count($result) == 0)
            return false;
        return true;
    }  
  
     /**
     * satisfactionary Questionnaires      
     * Get records from questionnaire_brand table                                      
     * Check1: Is Brand ID is Assigned
     * Check2: first argument is report, Reason - same  function is used in report also
     * check3: only fetch brands as per the User Type logins 
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/   

    public function fetch($args) {
           $BrandID = isset($args['firstArg'])?$args['firstArg']:''; 
           //check if first argment is for report
           if ($BrandID=="report")$BrandID = isset($args['secondArg'])?$args['secondArg']:''; 
           //assaign brand IDs list
           if($BrandID=='' && isset($_POST['BrandArr'])){               
               $list=rtrim($_POST['BrandArr'],',');                
               $args['where'] = "t1.BrandID IN (".$list.")";
           }
           if($BrandID!='')$args['where'] = "t1.BrandID='".$BrandID."'";
            $dbColumns = array('t1.QuestionnaireBrandID', 't1.BrandID', 't2.BrandName', 'date_format(t1.CreatedDate,"%d/%m/%Y")', 'date_format(t1.TerminationDate,"%d/%m/%Y")', 't1.Status');
            $tables    = "questionnaire_brand AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID ";
  
           //$dbColumns = array('QuestionnaireBrandID', 'BrandID', 'CreatedDate' , 'Status' );
           $table = 'questionnaire_brand';
           $output = $this->ServeDataTables($this->conn, $tables, $dbColumns, $args);     
           return  $output;        
     } 
    
     
     /**
     * satisfactionary Questionnaires      
     * Report Generation                                      
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/        
    public function generateReports($id) {  
        //get questinnaire ID and job ID from questionnaire table with for given questinnaire brand id
        
        $sql ="     SELECT t2.QuestionnaireID, t2.JobID, date_format(t2.ResponseDate,'%d-%m-%Y')  AS SurveyDate,
                    date_format(t1.CreatedDate,'%d-%m-%Y')  AS StartDate,
                    date_format( IFNULL( t1.TerminationDate, CURDATE( ) ) ,'%d-%m-%Y') AS EndDate
                            FROM questionnaire_brand AS t1                            
                    LEFT JOIN questionnaire AS t2 ON t1.BrandID = t2.QuestionnaireBrandID
                            WHERE t1.QuestionnaireBrandID = :ID
                            AND (
                                    date_format( t2.ResponseDate, '%Y-%m-%d' )
                                    BETWEEN t1.CreatedDate
                                    AND IFNULL( t1.TerminationDate, CURDATE( ) )
                                )
                    ORDER BY  t2.ResponseDate            
               ";
        $params = array( 'ID' => $id );
        $result = $this->Query($this->conn, $sql, $params);        
        if(empty($result))return $result;
    
        $job=$this->loadModel('Job'); 
        foreach($result as $value){
            $key=$value['QuestionnaireID'];
            $report[$key]['JobID']=$value['JobID'];
            $report[$key]['SurveyDate']=$value['SurveyDate'];             
            
            //get details of job using job id and job class
            $jobDetail= $job->fetch($value['JobID']);            
            
            $jobDetail['DateBooked'] = date_create_from_format('Y-m-d', $jobDetail['DateBooked']);
            $jobDetail['DateBooked'] = date_format($jobDetail['DateBooked'], 'd-m-Y');           
            $report[$key]['DateBooked']=$jobDetail['DateBooked'];
            
            $report[$key]['ServiceAgent']=$jobDetail['ServiceCentreName'];
            $report[$key]['Product']=$jobDetail['UnitTypeName'];
            $report[$key]['CustomerName']=$jobDetail['ContactTitle'].' '.$jobDetail['ContactFirstName'].' '.$jobDetail['ContactLastName'];
            $report[$key]['BrandName']=$jobDetail['Brand'];
            $report[$key]['ManufactureName']=$jobDetail['ManufacturerName'];
            //end getting job detials
            $attrValue=$this->getAttributeDataContent(1,$key,$id,false );
             
            $report[$key]['BrandScore']= $attrValue['NumericValue'];
            //attr_2
            $attrValue=$this->getAttributeDataContent(2,$key,$id,false );     
            $report[$key]['ManfactureScore']= $attrValue['NumericValue'];
            //attr_3
            $attrValue=$this->getAttributeDataContent(3,$key,$id,false );
            $report[$key]['ServiceAgentScore']= $attrValue['NumericValue'];
            //attr 4
            $attrValue=$this->getAttributeDataContent(4,$key,$id,true );           
            $report[$key]['AddlQuesOne']= $attrValue['AttributeValue'];
            $report[$key]['AddlQuesOneScore']= $attrValue['NumericValue'];
            //attr 5
            $attrValue=$this->getAttributeDataContent(5,$key,$id,true );           
            $report[$key]['AddlQuesTwo']= $attrValue['AttributeValue'];
            $report[$key]['AddlQuesTwoScore']= $attrValue['NumericValue'];
            //attr 6
            $attrValue=$this->getAttributeDataContent(6,$key,$id,true );           
            $report[$key]['AddlQuesThree']= $attrValue['AttributeValue'];
            $report[$key]['AddlQuesThreeScore']= $attrValue['NumericValue'];
            //attr 7
            $attrValue=$this->getAttributeDataContent(7,$key,$id,true );           
            $report[$key]['AddlQuesFour']= $attrValue['AttributeValue'];
            $report[$key]['AddlQuesFourScore']= $attrValue['NumericValue'];
            //attr 8
            $attrValue=$this->getAttributeDataContent(8,$key,$id,true );           
            $report[$key]['AddlQuesFive']= $attrValue['AttributeValue'];
            $report[$key]['AddlQuesFiveScore']= $attrValue['NumericValue'];
            //attr 10
            $attrValue=$this->getAttributeDataContent(10,$key,$id,true );           
            $report[$key]['ServiceRecomandBy']= $attrValue['StringValue'];         
        }
     
       return $report;
     } 
     /**
     * satisfactionary Questionnaires      
     * Get Start Date and End Date using in report generation                                     
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/         
     public function generateReportsDateRange($id){
           $sql ="     SELECT date_format(CreatedDate,'%d-%m-%Y') AS StartDate,
                              date_format( IFNULL( TerminationDate, CURDATE( ) ) ,'%d-%m-%Y') AS EndDate
                       FROM questionnaire_brand 
                       WHERE QuestionnaireBrandID = :ID
               ";
        $params = array( 'ID' => $id );
        $result = $this->Query($this->conn, $sql, $params); 
        return $result[0];
     }
     /**
     * satisfactionary Questionnaires      
     * 
     * this funtion is used in report generation for questionaries...
     * Questionnaire Brand Attributes values                                      
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/       
    public function getAttributeDataContent($AttrId, $QuesId, $QuesBrandID, $content=true){
        if($content){
            $sql ="
                SELECT t1.StringValue,t1.NumericValue,t1.TextValue, t2.AttributeValue
                    FROM 
                             questionnaire_data AS t1 
                             LEFT JOIN questionnaire_brand_attribute AS t2 ON t1.AttributeID = t2.QuestionnaireAttributeID                          
                    WHERE    t2.QuestionnaireBrandID = :QuesBrandID AND 
                             t1.AttributeID = :AttrId  AND
                             t1.QuestionnaireID = :QuesId
              ";  
        $params = array( 'QuesId' => $QuesId , 'AttrId' => $AttrId, 'QuesBrandID' => $QuesBrandID );
        $result = $this->Query($this->conn, $sql, $params);
        return  $result[0];
        }
        else
        {
          $sql ="
                SELECT * FROM `questionnaire_data`
                    WHERE QuestionnaireID = :QuesId AND 
                          AttributeID = :AttrId  
              ";  
        $params = array( 'QuesId' => $QuesId , 'AttrId' => $AttrId);
        $result = $this->Query($this->conn, $sql, $params);
        
                if(empty($result['0'])) {

                                   return  array( 
                                                   'QuestionnaireDataID' => '0',
                                                   'QuestionnaireID' => '0',                                 
                                                   'AttributeID' => '0',
                                                   'StringValue' => '0',
                                                   'NumericValue' => '0',
                                                   'TextValue' => '0'
                                                   );
               } else {
               return $result[0];
               }
        
        }
    }
     /**
     * satisfactionary Questionnaires 
     * 
     * If Implementation Date is equal to Current Date Then change the status to        * 
     * Inactive of privious questionnairs which is less than implementation date        * 
     * @return nothing
     * 
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/
     public function cronStatussChangeQuestionnaireBrand(){
         //SELECT BRAND ID which is having created date current date
         $sql ="
                SELECT  BrandID
                        FROM `questionnaire_brand`
                        WHERE `CreatedDate` = curdate( )
                ";
          $result = $this->Query($this->conn, $sql);
          
          if(count($result)>0)
          {
              $sql ="
                        UPDATE questionnaire_brand
                                    SET Status          ='In-active', 
                                    TerminationDate     = SUBDATE( CURDATE( ) , 1 )
                        WHERE CreatedDate < CURDATE( )
                                    AND 
                                        BrandID = :BrandID  
              ";  
              $update=$this->conn->prepare($sql);
              foreach($result as $brand){                
              $BrandID=$brand['BrandID'];  
              $params = [ 'BrandID' => $BrandID ];
              $update->execute($params);              
              }  
          }
     }
     
     /**
     * satisfactionary Questionnaires 
     * 
     * get Question detals and its attribute values                                
     * from  questionnaire_brand and its attributes in questionnaire_brand_attribute   
     *                                         
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/    
    public function getQuestionnaireAttribute($id){
        $sql = " SELECT  
                        t1.Status AS status, 
                        date_format(t1.CreatedDate,'%d/%m/%Y') AS implement_date, 
                        date_format(t1.TerminationDate,'%d/%m/%Y') AS termination_date, 
                        t2.BrandName AS brand FROM questionnaire_brand AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID 
                        WHERE t1.QuestionnaireBrandID=:ID 
               ";                     
        $params = array( 'ID' => $id );
        $result = $this->Query($this->conn, $sql, $params);
        $datarow = array();        
        $datarow = $result[0];
        
        $sql = " SELECT  
                        t1.AttributeName,                         
                        t2.AttributeValue FROM questionnaire_attribute AS t1 LEFT JOIN questionnaire_brand_attribute AS t2 ON t1.QuestionnaireAttributeID=t2.QuestionnaireAttributeID 
                        WHERE t2.QuestionnaireBrandID=:ID 
               ";                     
        $params = array( 'ID' => $id );
        $result = $this->Query($this->conn, $sql, $params);    
        foreach($result as $key => $value)
        {
             $datarow[$value['AttributeName']]=$value['AttributeValue'];
        }
  
        /*$datarow = array(
                    'brand'                             => 'Samsung', 
                    'ease_of_booking'                   => 'Ease of booking', 
                    'speed_of_service_first_visit'      => 'Speed of service (to first visit)', 
                    'speed_of_service_overall'          => 'Speed of service (overall)', 
                    'communication'                     => 'Communication throughout', 
                    'engineer'                          => 'Engineer (where relevant)', 
                    'Who_suggested_our_services_to_you' => 'Retailer|Insurer/Extended Warrantor|Advertisement|Manufacturer|Web Search|Personal Recommendation',
                    'implement_date'                    => '03/07/2013',
                    'termination_date'                  => '',
                    'status'                            => 'Active'
                    );
         * 
         */
        return $datarow;
    }
     /**
     * satisfactionary Questionnaires 
     * 
     * this function will get correct questionnaire form for  respective Brand                                  
     *                                         
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/    
    
    public function getRequiredQuestionnair($BrandID){
        $sql="
            SELECT `QuestionnaireBrandID`
                                FROM `questionnaire_brand`
                    WHERE `BrandID` = :ID
                                AND `Status` = 'Active'
                                AND `CreatedDate` <= CURDATE( )
                    ORDER BY `CreatedDate` DESC
             ";
        $params = array( 'ID' => $BrandID );
        $result = $this->Query($this->conn, $sql, $params); 
        return $result[0]['QuestionnaireBrandID'];
        
    }
    
    /**
     * satisfactionary Questionnaires 
     * 
     * this function will insert records in questionnaire_brand and its attributes                                   
     * in questionnaire_brand_attribute
     *                                          
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/    
    
    public function processData($args){
        extract($_POST);
      //Check questionary ID is exiting for BRAND
        $sql = "select * from questionnaire_brand where BrandID=:ID  ";
        $params = array( 'ID' => $SQbId );
        $result = $this->Query($this->conn, $sql, $params);        
      //if existing check the same data is submitted.        
        $chk=0;
        if ( count($result) > 0 ){
            if (!in_array("Retailer", $attr_10)) $chk=1;
            if (!in_array("Insurer/Extended Warrantor", $attr_10)) $chk=1;
            if (!in_array("Advertisement", $attr_10)) $chk=1;
            if (!in_array("Manufacturer", $attr_10)) $chk=1;
            if (!in_array("Web Search", $attr_10)) $chk=1;
            if (!in_array("Personal Recommendation", $attr_10)) $chk=1;
            if ($attr_4!="Ease of booking" 
                    || $attr_5!="Speed of service (to first visit)" 
                    || $attr_6!="Speed of service (overall)"
                    || $attr_7!="Communication throughout"
                    || $attr_8!="Engineer (where relevant)"
                )$chk=1;
            
            
        }
        else{           
            $chk=1;
        }
        //if it is not same data insert the new record or Brand has not Questionaire
        if($chk==1){
        //inserted record in questionnaire_brand and get the QuestionnaireBrandID done by srinivas
	$q =  "	INSERT INTO questionnaire_brand
			    (   BrandID,
                                Status,
                                CreatedDate
                             )
		VALUES      
                            (    
                                :BrandID,
                                :Status,                                
                                :CreatedDate
                                )
	     ";
        $implementationDate = date_create_from_format('d/m/Y', $implementationDate);
        $implementationDate = date_format($implementationDate, 'Y-m-d');
        $values = [	   
            "BrandID"           =>  $SQbId,
            "Status"            =>  "Active",
            "CreatedDate"       =>  $implementationDate
	];   
        
	$result = $this->execute($this->conn, $q, $values); 
        $QuestionnaireBrandID=$this->conn->lastInsertId();  
        //end of getting QuestionnaireBrandID
       
	$sql = "INSERT INTO questionnaire_brand_attribute
			    (
				QuestionnaireBrandID,
				QuestionnaireAttributeID,
				AttributeValue
			    )
		VALUES
			    (
				:QuestionnaireBrandID,
				:QuestionnaireAttributeID,
				:AttributeValue				
			    )
	     ";
        
	$values = [
            "4"             =>      $attr_4,
            "5"             =>      $attr_5,
            "6"             =>      $attr_6,
            "7"             =>      $attr_7,
            "8"             =>      $attr_8,
            "10"            =>      implode('|',$attr_10)                       
        ];
       
        $result =  $this->conn->prepare($sql);
        foreach($values as $key => $value) {
            $ins = [
                        "QuestionnaireBrandID"          => $QuestionnaireBrandID,
                        "QuestionnaireAttributeID"      => $key,
                        "AttributeValue"                => $value  
                   ];
             
             $result->execute($ins); 
            }
                
	
        
        }
      
          return array('status' => 'OK',
                        'message' => 'Your data has been inserted successfully.');
        
      
    }
    
}