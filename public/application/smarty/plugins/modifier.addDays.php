<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
 
 
/**
 * Smarty custom plugin to add a integer to a date and returns the date in UK format dd/mm/yyyy
 *
 * Type:     modifier<br>
 * Name:     addDays<br>
 * Purpose:  adds a specified number days to a date
 * @author   Dana Murad
 * @param string
 * @param integer
 * @return string
 */
function smarty_modifier_addDays($string,$numOfDays)
{
    list($month,$day,$year) = explode("/",$string); 
    $date = mktime(0,0,0,$month,$day+$numOfDays,$year);
    return date("m/d/Y",$date); 
}
 
?>