<style>
    .SystemAdminFormPanel .fieldLabel {
padding-top: 0px;
}
    </style>
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <div id="satisfactionQuestionnaireFormPanel" class="SystemAdminFormPanel" >
    
                <form id="SQForm" name="SQForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
           
                       
                <p>
                            <label class="fieldLabel" for="Name" >{$page['Labels']['brand_name']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.brand}
                        
                </p>
                {if isset($datarow.Who_suggested_our_services_to_you)}{assign var=radio value="|"|explode:$datarow.Who_suggested_our_services_to_you}          
               <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_1']|escape:'html'}" >{$page['Labels']['radio_buttoin_1']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$radio[0]}
                         </p>       

                
                <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_2']|escape:'html'}" >{$page['Labels']['radio_buttoin_2']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$radio[1]}
                         </p>                  
                         

                 <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_3']|escape:'html'}" >{$page['Labels']['radio_buttoin_3']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$radio[2]}
                         </p>   
                
                 <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_4']|escape:'html'}" >{$page['Labels']['radio_buttoin_4']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$radio[3]}
                         </p>                 
                  <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_5']|escape:'html'}" >{$page['Labels']['radio_buttoin_5']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$radio[4]}
                         </p>   
                  <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_6']|escape:'html'}" >{$page['Labels']['radio_buttoin_6']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$radio[5]}
                         </p>    
                     {/if}    
                     <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_1']|escape:'html'}" >{$page['Labels']['additiona_question_1']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.ease_of_booking}
                         </p> 
                         
                     <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_2']|escape:'html'}" >{$page['Labels']['additiona_question_2']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.speed_of_service_first_visit}
                         </p> 
                    <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_3']|escape:'html'}" >{$page['Labels']['additiona_question_3']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.speed_of_service_overall}
                         </p>      
                   <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_4']|escape:'html'}" >{$page['Labels']['additiona_question_4']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.communication}
                         </p>
                      <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_5']|escape:'html'}" >{$page['Labels']['additiona_question_5']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.engineer}
                         </p>
                      
                         <p>
                            <label class="fieldLabel" for="{$page['Labels']['implementation_date']|escape:'html'}" >{$page['Labels']['implementation_date']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.implement_date} 
                         </p>
                         
                           <p>
                            <label class="fieldLabel" for="{$page['Labels']['termination_date']|escape:'html'}" >{$page['Labels']['termination_date']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.termination_date}
                         </p>
                         
                           <p>
                            <label class="fieldLabel" for="{$page['Labels']['status']|escape:'html'}" >{$page['Labels']['status']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; {$datarow.status}
                         </p>


                                    
                                        
                              <p>
                    
                                <span class= "bottomButtons" >            
                                    
                                     <br>
                                        
                                        <input type="submit" name="cancel_btn" class="textSubmitButton centerBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >
                                        <br>
                                   
                                        



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
  