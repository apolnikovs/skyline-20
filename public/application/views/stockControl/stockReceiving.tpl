{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Stock"}
    {$PageId = $StockOrderingPage}
    {$fullscreen=true}
    {$showTopLogoBlankBox=false}
    {$controller="StockControl"}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/autoComplete.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
{/block}


{block name=scripts}

<style>
.row_selectedRed td{
 color:#fff;
 background:#E42019!important;
 }
</style>
   
 <script>
   
   
        
        
        
        var oldValue;
    $(document).ready(function() {
    
    var options, a;
jQuery(function(){
   options = { 
   serviceUrl:'{$_subdomain}/stockControl/getALLSPOrders/',
   minChars:1,
delimiter: /(,|;)\s*/, // regex or character
maxHeight:400,
width:300,
zIndex: 9999,
deferRequestBy: 0, //miliseconds
noCache: false, //default is false, set to true to disable caching
// callback function:
onSelect: function(value, data){ 



}


};
   a = $('#OrderNoFilter').autocomplete(options);
});
    
    
    $('th input:checkbox').click(function(e) {

var table = $(e.target).closest('table');
$('td input:checkbox', table).attr('checked', e.target.checked);

});
    var oTable = $('#StockResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		
    "sAjaxSource": "{$_subdomain}/{$controller}/loadStockReceivingTable/spid="+$('#serviceProviderSelect').val()+"/suplierID="+$('#supplierSelect').val()+"/OrderNoFilter="+$('#OrderNoFilter').val()+"/",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#StockResults').show();
                               
                               
			} );
                        },
                        "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page",
                                "sSearch": "Search within results"
                            },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[  25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,

"aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            {if $er==8}{$vis=0}{/if}
                            {if $er==9}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               
                               { "bVisible":1,"bSortable":false,"sWidth":'10px' }
                            
		]
                ,
	    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
           // console.log(aData);
           
		if(!aData[10]) {
		    $(nRow).css("color","#E42019");
		    $(nRow).addClass("redRowerBd13");
		}
	    }
               
   
 
        
          
});//datatable end

  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#StockResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
			$(this.nTr).removeClass('row_selectedRed');
                       
		});
		$(event.target.parentNode).addClass('row_selected');
               
                
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
        
        
        
 
      /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
                        
                      if (  $(aTrs[i]).hasClass('redRowerBd13') ){
                        $(".row_selected").addClass("row_selectedRed");
                       }
                       
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



   
    
    
    
    
	
        var v=0;
	
	                           
          
          
          
          
                          
             




	
		
  $('#serviceProviderSelect, #supplierSelect, #OrderNoFilter').change( function() {
        
    
                 $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockReceivingTable/spid="+$('#serviceProviderSelect').val()+"/suplierID="+$('#supplierSelect').val()+"/OrderNoFilter="+$('#OrderNoFilter').val()+"/"+$('input[name=orderStatuFilter]:checked').val()+"/");
       
	} );               
		
  $('input[name=orderStatuFilter]').click( function() {
        
    
                 $('#tt-Loader').show();
                                $('#StockResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/{$controller}/loadStockReceivingTable/spid="+$('#serviceProviderSelect').val()+"/suplierID="+$('#supplierSelect').val()+"/OrderNoFilter="+$('#OrderNoFilter').val()+"/"+$('input[name=orderStatuFilter]:checked').val()+"/");
       
	} );               
              
//receiving part modal window
 /* Add a click handler for the orderStock button row */
	$('#ReceivePartsButton').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                       href:"{$_subdomain}/{$controller}/ReceivePartsForm/id="+anSelected[0].id,
                       
                        opacity: 0.75,
                       
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );   
} );//doc ready

function ReceiveTaggedParts(){
parts=new Array();
orders=new Array();
r=$.each($('#StockResults td input:checked'),function( key, value ) {
  parts.push($(this).closest('tr').attr('id'));
  orders.push($(this).closest('tr').children('td:eq(0)').html());
  
});
var sorted_arr = orders.sort(); // You can define the comparing function here. 
                             // JS by default uses a crappy string compare.
 var unq=true;

for (var i = 0; i < orders.length - 1; i++) {
    if (sorted_arr[i + 1] != sorted_arr[i]) {
       var unq=false;
    }
}

if(!unq){
//more then one order select
{$legend="legend=Receive Tagged Parts"}
{$text="text=Multiple parts can only be received on the same order. Please check which items are tagged and try again." }
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/oneBut=true/",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}else{
{$legend="legend=Receive Tagged Parts"}
{$text="text=Fields marked with a * are compulsory"}
{$inputLabel="inputLabel=Supplier Invoice No:"}
{$inputID="inputID=InvoiceNoInput"}
{$confButAction="confButAction=validateInvoice()"}
$.colorbox({ 
 
                        href:"{$_subdomain}/Popup/showGenericCBPromt/{$legend|escape:'url'}/{$text|escape:'url'}/{$confButAction|escape:'url'}/{$inputID|escape:'url'}/{$inputLabel|escape:'url'}",
                        title: "Confirmation",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false,
                        onComplete:function(){
                        $('#RequisitionNoInput').focus().select();
                        }

                });

}



}
    
function validateInvoice(){
if($('#InvoiceNoInput').val()==""){
 $('#validationMsgP').html("Supplier Invoice No Is Required")
        $('#InvoiceNoInput').focus().select();
        $.colorbox.resize();
        }else{
        parts=new Array();
r=$.each($('#StockResults td input:checked'),function( key, value ) {
  parts.push($(this).closest('tr').attr('id'));
  
  
});
         $.post("{$_subdomain}/{$controller}/ReceiveMultiPart",{ invoiceNo:$('#InvoiceNoInput').val(),items:parts },
function(data) {
$.colorbox.close();
oTable = $('#StockResults').dataTable();
oTable.fnReloadAjax();
});

        }
}


function showGRNForm(){
$.colorbox({ 
 
                        href:"{$_subdomain}/{$controller}/GRNForm",
                        title: "Print GRN",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false,
                        onComplete:function(){
                        $('#RequisitionNoInput').focus().select();
                        }

                });

}

    </script>

    
{/block}


{block name=body}
 <style>
.autocomplete-w1 { background:url(img/shadow.png) no-repeat bottom right; position:absolute; top:0px; left:0px; margin:6px 0 0 6px; /* IE6 fix: */ _background:none; _margin:1px 0 0 0; }
.autocomplete-suggestions { border:1px solid #999; background:#FFF!important; cursor:default; text-align:left; max-height:350px; overflow:auto;  /* IE6 specific: */ _height:350px;  _margin:0; _overflow-x:hidden; }
.autocomplete-suggestions .selected { background:#F0F0F0; }
.autocomplete-suggestions div { padding:2px 5px; white-space:nowrap; overflow:hidden; }
.autocomplete-suggestions strong { font-weight:normal; color:#3399FF; }
.autocomplete-suggestion:hover{ background:#336699 ; cursor:pointer;color:#fff }


.dataTables_filter{ margin-top:38px;}
</style>
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/index/siteMap/" >{$page['Text']['SiteMap']}</a> / {$page['Text']['StockReceiving']}

        </div>
    </div>
           
    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="StockTopForm" name="StockTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['StockReceivingTable']}</legend>
                        <p>
                            <label>{$page['Text']['legend_text']}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  
                {if !isset($NotPermited)}        
 <div class="ServiceAdminResultsPanel" id="StockResultsPanel" >
                    

                 
                    
     
                    
                    <form id="StockResultsForm" class="dataTableCorrections">
                        <div style="float:left">
                        {if isset($splist)}
                            <div style="float:left">
                        <select id="serviceProviderSelect" style="float:left;padding: 4px;position:relative;top:-2px">
                            <option value="0">Select Service Provider</option>
                        {foreach $splist as $s}
                            <option value="{$s.ServiceProviderID}">{$s.Acronym}</option>
                        {/foreach}
                        </select>
                        </div>
                        {/if}
                        
                        {if isset($suppliers)}
                            <p style="float:left">
                        <select name="supplierSelect" id="supplierSelect" style="float:left;padding: 4px;position:relative;top:-2px">
                            <option value="0">Select Supplier</option>
                        {foreach $suppliers as $s}
                            <option value="{$s.ServiceProviderSupplierID}">{$s.CompanyName}</option>
                        {/foreach}
                        </select>
                        </p>
                        {/if}
                        
                        <p style="float:left">
                        <label class="cardLabel" style="width:58px">Order No: </label><input style="width:233px" type="text" name="OrderNoFilter" id="OrderNoFilter" value="">
                        </p>
                        </div>
                        
                        
                        <div style="float:left;margin-left: 200px;">
                        <p>
                            <input type="radio" name="orderStatuFilter" checked="checked" value="Outstanding"> Outstanding Orders 
                       </p>
                       <p>
    
                            <input type="radio" name="orderStatuFilter" value="Received"> Received Orders 
                       </p>
                       <p>    
                            <input type="radio" name="orderStatuFilter" value="All"> All Orders 
                        </p>
                        </div>
                        <table id="StockResults" style="display:none" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="width:10px;text-align: center"><input type="checkbox"></th>
			    </tr>
                        </thead>
                        <tbody>
                           <div style="text-align:left" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                        </tbody>
                    </table>  
                    <input type="hidden" name="sltSPs" id="sltSPs" >            
                    </form>
                    
                    
                    
                </div>        

                <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:900px">
                        <div >
<!--                            <button type="button" id="edit"  class="gplus-blue">{$page['Buttons']['edit']|escape:'html'}</button>
                            <button type="button" onclick="stockInsert()" class="gplus-blue">{$page['Buttons']['insert']|escape:'html'}</button>
                            <button type="button" id="delete" class="gplus-red">{$page['Buttons']['delete']|escape:'html'}</button>-->
                            <p style="float:left">
                            
                            
                            <button id="ReceivePartsButton" style="width:150px" type="button"  class="gplus-blue">{$page['Buttons']['ReceiveParts']|escape:'html'}</button>
                            <button id="ReceiveTaggedParts" style="width:150px" type="button" onclick="ReceiveTaggedParts()" class="gplus-blue">{$page['Buttons']['ReceiveTaggedParts']|escape:'html'}</button>
                            <button style="width:150px" type="button" onclick="showGRNForm()" class="gplus-blue">{$page['Buttons']['PrintGRN']|escape:'html'}</button>
                          </p>
                           
                            
                        </div>
<!--                            <div id="buttonSpacer" style="width:100px;float:left">&nbsp;</div><button type="button" id="stockHistoryBut" class="gplus-blue">Stock History</button>-->
                    </div>
                  
<!--                    <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                  
                    </div>-->
             
                  
                </div>        
                <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div> 

                

                {if $SuperAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               
             {/if}   <!-- end of notpermited-->

    </div>
                <div style="float: left;width: 100%;top: 12px;position: relative;">
                <hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/index/siteMap'">Finish</button>
                </div>
    

                
 
                
               

{/block}



