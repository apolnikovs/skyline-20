<?php

/* Note:
   To time the execution of this program uncomment the follwing
   timer code and similar code at the end of this file.
   Execution time will be recorded in /less.text
***********************************************************/
//$time_start = microtime(true);

if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start("ob_gzhandler");
} else {
    ob_start();
}

$skin = isset($_GET['skin']) ? $_GET['skin'] : 'default';
$size = isset($_GET['size']) ? $_GET['size'] : '950';

//Base skin is loaded from /css/Base

$files = [
    "Base/lessblue/reset.css",
    "Base/lessblue/config_{$size}.less", 
    "Base/variables.less", 
    
    "Base/lessblue/mixins.less",
    "Base/lessblue/typography.less",
    "Base/lessblue/forms.less",
    "Base/lessblue/grid.less",
    "Base/lessblue/tables.less",
    "Base/lessblue/extras.less",
    
    "Base/datatables.less",
    "Base/email.less",
    "Base/jobupdate.less",
    "Base/timeline.less",
    "Base/screen.less", 
    "Base/sitemap.less", 
    "Base/diary.less" 
];

$cache = "cache/base_{$size}.css"; 

$time = mktime(0, 0, 0, 21, 5, 1980);

foreach ($files as $file) {
    $fileTime = filemtime($file);

    if ($fileTime > $time) {
        $time = $fileTime;
    }
}

if (file_exists($cache)) {
    $cacheTime = filemtime($cache);
    if ($cacheTime < $time) {
        $time = $cacheTime;
        $recache = true;
    } else {
        $recache = false;
    }
} else {
    $recache = true;
}

if (!$recache && isset($_SERVER['If-Modified-Since']) && strtotime($_SERVER['If-Modified-Since']) >= $time) {
    header("HTTP/1.0 304 Not Modified");
} else {
    if ($recache) {
        require 'lessc.inc.php';
        $lc = new lessc();

        $css = '';
        
        foreach ($files as $file) {
            $css .= file_get_contents($file);
        }

        try {
            $css = $lc->parse($css);
        } catch (Exception $ex) {
            exit('lessc fatal error:<br />'.$ex->getMessage());
        }
        file_put_contents($cache, $css);
    } 
    
    header('Content-type: text/css');
    header('Last-Modified: ' . gmdate("D, d M Y H:i:s", $time) . " GMT");  
    readfile($cache);   
}


/* Note:
   To time the execution of this program uncomment the follwing
   timer code and similar code at the start of this file.
   Execution time will be recorded in /less.text
***********************************************************/
//$time_end = microtime(true);
//$time = round($time_end - $time_start, 4);
//file_put_contents('less.txt',"less compile time = $time\n",FILE_APPEND);

?>
