# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.136');

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer_details"                          #
# ---------------------------------------------------------------------- #

CREATE TABLE IF NOT EXISTS `service_provider_engineer_details` (
  `ServiceProviderEngineerDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderEngineerID` int(11) NOT NULL,
  `WorkDate` date NOT NULL,
  `StartShift` time NOT NULL,
  `EndShift` time NOT NULL,
  `StartPostcode` varchar(8) NOT NULL,
  `EndPostcode` varchar(8) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`ServiceProviderEngineerDetailsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# ---------------------------------------------------------------------- #
# Drop table "service_provider_engineer_skillset_day"                    #
# ---------------------------------------------------------------------- #

DROP TABLE `service_provider_engineer_skillset_day`;

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer_skillset_day"                     #
# ---------------------------------------------------------------------- #

CREATE TABLE IF NOT EXISTS `service_provider_engineer_skillset_day` (
  `ServiceProviderEngineerSkillsetDayID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderEngineerDetailsID` int(11) NOT NULL,
  `ServiceProviderSkillsetID` int(11) NOT NULL,
  PRIMARY KEY (`ServiceProviderEngineerSkillsetDayID`),
  UNIQUE KEY `IDX_service_provider_engineer_skillset_day_1` (`ServiceProviderEngineerDetailsID`,`ServiceProviderSkillsetID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer"                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer` ADD `SentToViamente` ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'Yes';
ALTER TABLE `service_provider_engineer` ADD `Deleted` ENUM( 'No', 'Yes' ) NOT NULL DEFAULT 'No';


# ---------------------------------------------------------------------- #
# Add table "service_provider"                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD `SetupUniqueTimeSlotPostcodes` ENUM( 'No', 'Yes' ) NOT NULL DEFAULT 'No';
ALTER TABLE `service_provider` ADD `UnlockingPassword` VARCHAR(64) NULL;
ALTER TABLE `service_provider` ADD `PasswordProtectNextDayBookings` ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No';
ALTER TABLE `service_provider` ADD `PasswordProtectNextDayBookingsTime` TIME NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.137');