# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.225');


# ---------------------------------------------------------------------- #
# Modify Table manufacturer                                              #
# ---------------------------------------------------------------------- #

ALTER TABLE `manufacturer`
ADD COLUMN `AccountNo` VARCHAR(20) NULL AFTER `ModifiedDate`,
ADD COLUMN `AddressLine1` VARCHAR(60) NULL AFTER `AccountNo`,
ADD COLUMN `AddressLine2` VARCHAR(60) NULL AFTER `AddressLine1`,
ADD COLUMN `AddressLine3` VARCHAR(60) NULL AFTER `AddressLine2`,
ADD COLUMN `Postcode` VARCHAR(10) NULL AFTER `AddressLine3`,
ADD COLUMN `TelNoSwitchboard` VARCHAR(20) NULL AFTER `Postcode`,
ADD COLUMN `TelNoSpares` VARCHAR(20) NULL AFTER `TelNoSwitchboard`,
ADD COLUMN `TelNoWarranty` VARCHAR(20) NULL AFTER `TelNoSpares`,
ADD COLUMN `FaxNo` VARCHAR(20) NULL AFTER `TelNoWarranty`,
ADD COLUMN `EmailAddress` VARCHAR(255) NULL AFTER `FaxNo`,
ADD COLUMN `WarrantyEmailAddress` VARCHAR(255) NULL AFTER `EmailAddress`,
ADD COLUMN `SparesEmailAddress` VARCHAR(255) NULL AFTER `WarrantyEmailAddress`,
ADD COLUMN `ContactName` VARCHAR(30) NULL AFTER `SparesEmailAddress`,
ADD COLUMN `PrimarySupplierID` INT(11) NULL AFTER `ContactName`,
ADD COLUMN `StartClaimNo` VARCHAR(50) NULL AFTER `PrimarySupplierID`,
ADD COLUMN `VATNo` VARCHAR(20) NULL AFTER `StartClaimNo`,
ADD COLUMN `IRISManufacturer` ENUM('Yes','No') NULL AFTER `VATNo`,
ADD COLUMN `FaultCodesReq` ENUM('Yes','No') NULL AFTER `IRISManufacturer`,
ADD COLUMN `ExtendedGuaranteeNoReq` ENUM('Yes','No') NULL AFTER `FaultCodesReq`,
ADD COLUMN `PolicyNoReq` ENUM('Yes','No') NULL AFTER `ExtendedGuaranteeNoReq`,
ADD COLUMN `InvoiceNoReq` ENUM('Yes','No') NULL AFTER `PolicyNoReq`,
ADD COLUMN `CircuitRefReq` ENUM('Yes','No') NULL AFTER `InvoiceNoReq`,
ADD COLUMN `PartNoReq` ENUM('Yes','No') NULL AFTER `CircuitRefReq`,
ADD COLUMN `OriginalRetailerReq` ENUM('Yes','No') NULL AFTER `PartNoReq`,
ADD COLUMN `ReferralNoReq` ENUM('Yes','No') NULL AFTER `OriginalRetailerReq`,
ADD COLUMN `SerialNoReq` ENUM('Yes','No') NULL AFTER `ReferralNoReq`,
ADD COLUMN `FirmwareReq` ENUM('Yes','No') NULL AFTER `SerialNoReq`,
ADD COLUMN `WarrantyLabourRateReqOn` ENUM('Yes','No') NULL AFTER `FirmwareReq`,
ADD COLUMN `SageAccountNo` ENUM('Yes','No') NULL AFTER `WarrantyLabourRateReqOn`,
ADD COLUMN `NoWarrantyInvoices` ENUM('Yes','No') NULL AFTER `SageAccountNo`,
ADD COLUMN `DisplaySectionCodes` ENUM('Yes','No') NULL AFTER `NoWarrantyInvoices`,
ADD COLUMN `DisplayTwoConditionCodes` ENUM('Yes','No') NULL AFTER `DisplaySectionCodes`,
ADD COLUMN `DisplayJobPriceStructure` ENUM('Yes','No') NULL AFTER `DisplayTwoConditionCodes`,
ADD COLUMN `DisplayTelNos` ENUM('Yes','No') NULL AFTER `DisplayJobPriceStructure`,
ADD COLUMN `DisplayPartCost` ENUM('Yes','No') NULL AFTER `DisplayTelNos`,
ADD COLUMN `DisplayRepairTime` ENUM('Yes','No') NULL AFTER `DisplayPartCost`,
ADD COLUMN `DisplayAuthNo` ENUM('Yes','No') NULL AFTER `DisplayRepairTime`,
ADD COLUMN `DisplayOtherCosts` ENUM('Yes','No') NULL AFTER `DisplayAuthNo`,
ADD COLUMN `DisplayAdditionalFreight` ENUM('Yes','No') NULL AFTER `DisplayOtherCosts`,
ADD COLUMN `NoOfClaimPrints` INT(11) NULL AFTER `DisplayAdditionalFreight`,
ADD COLUMN `EDIClaimNo` INT(11) NULL AFTER `NoOfClaimPrints`,
ADD COLUMN `EDIClaimFileName` VARCHAR(50) NULL AFTER `EDIClaimNo`,
ADD COLUMN `UseProductCode` ENUM('Yes','No') NULL AFTER `EDIClaimFileName`,
ADD COLUMN `UseMSNNo` ENUM('Yes','No') NULL AFTER `UseProductCode`,
ADD COLUMN `UseMSNFormat` ENUM('Yes','No') NULL AFTER `UseMSNNo`,
ADD COLUMN `MSNFormat` VARCHAR(20) NULL AFTER `UseMSNFormat`,
ADD COLUMN `WarrantyPeriodFromDOP` INT NULL AFTER `MSNFormat`,
ADD COLUMN `ValidateDateCode` ENUM('Yes','No') NULL AFTER `WarrantyPeriodFromDOP`,
ADD COLUMN `ForceStatusID` INT(11) NULL AFTER `ValidateDateCode`,
ADD COLUMN `POPPeriod` INT(11) NULL AFTER `ForceStatusID`,
ADD COLUMN `ClaimPeriod` INT(11) NULL AFTER `POPPeriod`,
ADD COLUMN `UseQA` ENUM('Yes','No') NULL AFTER `ClaimPeriod`,
ADD COLUMN `UsePreQA` ENUM('Yes','No') NULL AFTER `UseQA`,
ADD COLUMN `QAExchangeUnits` ENUM('Yes','No') NULL AFTER `UsePreQA`,
ADD COLUMN `QALoanUnits` ENUM('Yes','No') NULL AFTER `QAExchangeUnits`,
ADD COLUMN `CompleteOnQAPass` ENUM('Yes','No') NULL AFTER `QALoanUnits`,
ADD COLUMN `ValidateNetworkQA` ENUM('Yes','No') NULL AFTER `CompleteOnQAPass`,
ADD COLUMN `ExcludeFromBouncerTable` ENUM('Yes','No') NULL AFTER `ValidateNetworkQA`,
ADD COLUMN `BouncerPeriod` INT(11) NULL AFTER `ExcludeFromBouncerTable`,
ADD COLUMN `BouncerPeriodBasedOn` ENUM('BookedDate','CompletedDate','DespatchedDate') NULL AFTER `BouncerPeriod`,
ADD COLUMN `EDIExportFormatID` INT(11) NULL AFTER `BouncerPeriodBasedOn`,
ADD COLUMN `CreateTechnicalReport` ENUM('Yes','No') NULL AFTER `EDIExportFormatID`,
ADD COLUMN `CreateServiceHistoryReport` ENUM('Yes','No') NULL AFTER `CreateTechnicalReport`,
ADD COLUMN `IncludeOutOfWarrantyJobs` ENUM('Yes','No') NULL AFTER `CreateServiceHistoryReport`,
ADD COLUMN `WarrantyAccountNo` VARCHAR(20) NULL AFTER `IncludeOutOfWarrantyJobs`,
ADD COLUMN `TechnicalReportIncludeAdj` ENUM('Yes','No') NULL AFTER `WarrantyAccountNo`,
ADD COLUMN `PartNoReqForWarrantyAdj` ENUM('Yes','No') NULL AFTER `TechnicalReportIncludeAdj`,
ADD COLUMN `ForceAdjOnNoParts` ENUM('Yes','No') NULL AFTER `PartNoReqForWarrantyAdj`,
ADD COLUMN `ForceKeyRepairPart` ENUM('Yes','No') NULL AFTER `ForceAdjOnNoParts`,
ADD COLUMN `ForceOutOfWarrantyFaultCodes` ENUM('Yes','No') NULL AFTER `ForceKeyRepairPart`,
ADD COLUMN `WarrantyExchangeFree` DECIMAL(10,2) NULL AFTER `ForceOutOfWarrantyFaultCodes`,
ADD COLUMN `UseFaultCodesForOBFJobs` ENUM('Yes','No') NULL AFTER `WarrantyExchangeFree`,
ADD COLUMN `AutoFinalRejectClaimsNotResubmitted` ENUM('Yes','No') NULL AFTER `UseFaultCodesForOBFJobs`,
ADD COLUMN `DaysToResubmit` INT(11) NULL AFTER `AutoFinalRejectClaimsNotResubmitted`,
ADD COLUMN `LimitResubmissions` ENUM('Yes','No') NULL AFTER `DaysToResubmit`,
ADD COLUMN `LimitResubmissionsTo` INT(11) NULL AFTER `LimitResubmissions`,
ADD COLUMN `CopyDOPFromBouncerJob` ENUM('Yes','No') NULL AFTER `LimitResubmissionsTo`;

ALTER TABLE manufacturer ADD COLUMN BuildingNameNumber VARCHAR(50) NULL AFTER Status, 
						ADD COLUMN Street VARCHAR(100) NULL AFTER BuildingNameNumber, 
						ADD COLUMN LocalArea VARCHAR(100) NULL AFTER Street, 
						ADD COLUMN TownCity VARCHAR(100) NULL AFTER LocalArea, 
						ADD COLUMN CountryID INT(3) NULL AFTER TownCity, 
						ADD COLUMN TelNoTechnical VARCHAR(20) NULL DEFAULT NULL AFTER TelNoWarranty, 
						ADD COLUMN JobFaultCodeID INT NULL DEFAULT NULL AFTER CopyDOPFromBouncerJob, 
						ADD COLUMN PartFaultCodeID INT NULL DEFAULT NULL AFTER JobFaultCodeID;


ALTER TABLE manufacturer CHANGE COLUMN SageAccountNo SageAccountNo VARCHAR(20) NULL DEFAULT NULL AFTER WarrantyLabourRateReqOn;

# ---------------------------------------------------------------------- #
# Add Table associated_manufacturers                                     #
# ---------------------------------------------------------------------- #
CREATE TABLE associated_manufacturers ( 
										AssociatedManufacturersID INT(10) NOT NULL AUTO_INCREMENT, 
										ParentManufacturerID INT(10) NULL DEFAULT NULL, 
										ChildManufacturerID INT(10) NULL DEFAULT NULL, 
										PRIMARY KEY (AssociatedManufacturersID) 
									   ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;
									   
									   

# ---------------------------------------------------------------------- #
# Add Table job_accessory                                                #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS job_accessory (
											JobID int(11) NOT NULL, 
											AccessoryID int(11) NOT NULL, 
											UNIQUE KEY JobID (JobID,AccessoryID)
										) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# ---------------------------------------------------------------------- #
# Modify Table service_provider                                          #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider ADD SundayOpenTime TIME NULL AFTER SaturdayCloseTime, 
							 ADD SundayCloseTime TIME NULL AFTER SundayOpenTime;

# ---------------------------------------------------------------------- #
# Modify Table datatables_custom_columns                                 #
# ---------------------------------------------------------------------- #
 ALTER TABLE `datatables_custom_columns` ADD COLUMN `ColumnDisplayString` VARCHAR(500) NULL DEFAULT NULL,
										ADD COLUMN `ColumnOrderString` VARCHAR(500) NULL DEFAULT NULL,
										ADD COLUMN `ColumnNameString` VARCHAR(5000) NULL DEFAULT NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.226');
