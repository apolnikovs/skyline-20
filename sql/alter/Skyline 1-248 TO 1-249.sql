# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.248');

# ---------------------------------------------------------------------- #
# Modify branch Table                                                    #
# ---------------------------------------------------------------------- # 
ALTER TABLE `branch`
	CHANGE COLUMN `BranchType` `BranchType` ENUM('Store','Extra','Call Centre','Website','Mobile Network Store','Mobile Independent Store') NULL DEFAULT NULL AFTER `BranchNumber`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.249');
