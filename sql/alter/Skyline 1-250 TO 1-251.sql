# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.250');

# ---------------------------------------------------------------------- #
# Modify one_touch_audit Table                                           #
# ---------------------------------------------------------------------- # 
ALTER TABLE `one_touch_audit`
	ADD COLUMN `AvailiableDate` DATE NULL DEFAULT NULL AFTER `Day7PMAvailability`,
	ADD COLUMN `DiaryAllocationID` INT(11) NULL DEFAULT NULL AFTER `AvailiableDate`,
	ADD CONSTRAINT `diary_allocation_TO_one_touch_audit` FOREIGN KEY (`DiaryAllocationID`) REFERENCES `diary_allocation` (`DiaryAllocationID`);


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.251');
